<%@ Page Language="C#" MasterPageFile="~/AfterLogin.master" AutoEventWireup="true"
    CodeFile="MISpringVendorForm.aspx.cs" Inherits="MISpringVendorForm" Title="MI Spring Vendor Form" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headercontent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <h1 class="caption">VENDOR SIGN-UP FORM - TradeFirst Spring Barter Expo</h1>
    <h4 style="text-align:center" >Wednesday, March 20, 2019 | Southfield Civic Center | 4:30 p.m. � 9 p.m.</h4>
    <table class="width800">
        <tr>
            <td valign="top">
                <fieldset>
                    <legend>Sold By</legend>
                    <%--<asp:Label ID="lblSold" Text="Sold By" runat="server" class="formlabelright left"></asp:Label>--%>
                    <asp:TextBox ID="txtSold" CssClass="fld100" runat="server"></asp:TextBox>
                </fieldset>
            </td>
            <td width="50%" valign="top">
                <fieldset>
                    <legend>Account Executive</legend>                    
                    <asp:TextBox ID="txtAccountExec" runat="server" CssClass="fld200" ReadOnly="true"></asp:TextBox>
                    <asp:Label ID="lblAccountExecEmail" runat="Server" class="formlabelright" Visible="false"></asp:Label>
                </fieldset>
        </tr>

        <tr>
            <td width="50%" valign="top">
                <fieldset>
                    <legend>Booth preference</legend>
                    <asp:TextBox ID="txtCorner" runat="server" CssClass="fld100"></asp:TextBox>
                    <asp:Label ID="lblCorner" runat="Server" Text="corner booth(s) 8�x10� at $475.00 trade" class="formlabelright"></asp:Label><br />

                    <div style="text-align: center; color: darksalmon;">or</div>

                    <asp:TextBox ID="txtBooth" runat="server" CssClass="fld100"></asp:TextBox>
                    <asp:Label ID="lblBooth" runat="Server" Text="booth(s) 8�x10� at $425.00 trade" class="formlabelright"></asp:Label><br />

                    <asp:TextBox ID="txtElecticity" runat="server" CssClass="fld100"></asp:TextBox>
                    <asp:Label ID="lblElecticity" runat="Server" Text="I would like electricity $25 trade" class="formlabelright"></asp:Label><br />
                </fieldset>
            </td>
            <td width="50%" valign="top">
                <fieldset>
                    <legend>Table preference</legend>
                    <asp:TextBox ID="txtSize" runat="server" CssClass="fld100"></asp:TextBox>
                    <asp:Label ID="lblSize" runat="Server" Text="6� table or 8� table" class="formlabelright"></asp:Label><br />

                    <asp:TextBox ID="txtExtra" runat="server" CssClass="fld100"></asp:TextBox>
                    <asp:Label ID="lxlExtra" runat="Server" Text="extra table(s) 6� or 8� $45 trade" class="formlabelright"></asp:Label><br />
                </fieldset>
            </td>
        </tr>
        <tr>
            <td valign="top" width="50%">
                <fieldset>
                    <legend>Company Information</legend>
                    <asp:Label ID="lblCompany" runat="Server" Text="Company" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtCompany" runat="server" CssClass="fld200" ReadOnly="true"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblAccountNum" runat="Server" Text="Account Number" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtAccountNum" runat="server" CssClass="fld200" ReadOnly="true"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblContactPerson" runat="server" class="formlabelright left" Text="Contact Person"></asp:Label>
                    <asp:TextBox ID="txtContactPerson" CssClass="fld100" runat="server" />
                </fieldset>
            </td>
            <td width="50%" valign="top">
                <fieldset>
                    <legend>Company Information</legend>
                    <asp:Label ID="lblContact" runat="Server" Text="Phone #" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtContact" runat="server" CssClass="fld100"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblEmail" runat="Server" Text="E-Mail" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="fld200"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblBoothSign" runat="Server" Text="Booth Sign to Read" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtBoothSign" runat="server" CssClass="fld200"></asp:TextBox><br />
                </fieldset>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <fieldset>
                    <legend>Describe product you are bringing :</legend>
                    <asp:TextBox ID="NotesTextArea" TextMode="multiline" Columns="60" Rows="2" runat="server" Required/>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <fieldset style="padding-top: 0px; padding-bottom: 0px">
                    <p>
                        <strong>Great door prizes generate more traffic! What would you like to offer as a door prize to randomly selected winners on the day of the event? 
                        (Bring your item(s) on the day of the Expo and TradeFirst will organize the giveaway for you.)</strong>
                    </p>
                    <asp:TextBox ID="NotesPrizeArea" TextMode="multiline" Columns="60" Rows="2" runat="server" />
                </fieldset>
            </td>
        </tr>

        <tr>
            <td width="50%" valign="top">
                <fieldset>
                    <legend>Signature</legend>
                    <asp:Label runat="server" ID="lblName" Text="Name" CssClass="formlabelright left"></asp:Label>
                    <asp:TextBox runat="server" ID="txtName" CssClass="fld200" Required></asp:TextBox><br />
                    <asp:Label runat="server" ID="lblDate" Text="Date" CssClass="formlabelright left"></asp:Label>
                    <asp:TextBox runat="server" ID="txtdate" CssClass="fld100" Required></asp:TextBox>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Button ID="btnsave" runat="server" Text="Submit" OnClick="btnsave_click" CssClass="btn"
                    ToolTip="Submit" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <fieldset style="padding-top: 0px; padding-bottom: 0px">
                    <p style="display: inline;">
                        All products and services must be 100% trade to participate in any Barter Expo. Booth must be set up by 4:00pm. Booths
                        are assigned at the discretion of TradeFirst. Buyer�s signature acknowledges satisfactory receipt of product or services
                        described and gives authority to TradeFirst to debit buyer�s account. All rules and regulations apply. No shows will pay
                        for booth and be penalized an additional $200 if they have not notified TradeFirst three weeks prior to the show.
                    </p>
                </fieldset>
            </td>
        </tr>
    </table>

    <script type="text/javascript">

</script>

</asp:Content>
