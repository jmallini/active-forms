<%@ Page Language="C#" MasterPageFile="~/AfterLogin.master" AutoEventWireup="true"
    CodeFile="MI_Holiday_Restaurant_Form.aspx.cs" Inherits="MI_Holiday_Restaurant_Form" Title="MI Restaurant Sign-up Form" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headercontent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <h1 class="caption">RESTAURANT SIGN-UP FORM - TradeFirst Holiday Barter Expo</h1>
    <h4 style="text-align: center">Wednesday, November 13, 2019 | Southfield Civic Center | 4:30 pm � 9 pm</h4>
    <table class="width800">
        <tr>
            <td valign="top">
                <fieldset>
                    <legend>Sold By</legend>
                    <%--<asp:Label ID="lblSold" Text="Sold By" runat="server" class="formlabelright left"></asp:Label>--%>
                    <asp:TextBox ID="txtSold" CssClass="fld100" runat="server"></asp:TextBox>
                </fieldset>
            </td>
            <td width="50%" valign="top">
                <fieldset>
                    <legend>Account Executive</legend>
                    <asp:TextBox ID="txtAccountExec" runat="server" CssClass="fld200" ReadOnly="true"></asp:TextBox>
                    <asp:Label ID="lblAccountExecEmail" runat="Server" class="formlabelright" Visible="false"></asp:Label>
                </fieldset>
        </tr>
        <tr>
            <td valign="top" width="50%">
                <fieldset>
                    <legend>Company Information</legend>
                    <asp:Label ID="lblCompany" runat="Server" Text="Company" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtCompany" runat="server" CssClass="fld200" ReadOnly="true"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblAccountNum" runat="Server" Text="Account Number" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtAccountNum" runat="server" CssClass="fld200" ReadOnly="true"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblContactPerson" runat="server" class="formlabelright left" Text="Contact Person"></asp:Label>
                    <asp:TextBox ID="txtContactPerson" CssClass="fld100" runat="server" />
                </fieldset>
            </td>
            <td width="50%" valign="top">
                <fieldset>
                    <legend>Company Information</legend>
                    <asp:Label ID="lblContact" runat="Server" Text="Phone #" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtContact" runat="server" CssClass="fld100"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblEmail" runat="Server" Text="E-Mail" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="fld200"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblBoothSign" runat="Server" Text="Booth Sign to Read" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtBoothSign" runat="server" CssClass="fld200"></asp:TextBox><br />
                </fieldset>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <fieldset style="padding-top: 0px; padding-bottom: 0px">
                    <p>
                        <strong>Do you plan to offer a special, discount, raffle, or door prize for more traffic?. If yes, please explain:</strong>
                    </p>
                    <asp:TextBox ID="NotesPrizeArea" TextMode="multiline" Columns="60" Rows="2" runat="server" />
                </fieldset>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <fieldset>
                    <p><strong>I will be providing members with small portions of the following item at the show:</strong></p>
                    <asp:TextBox ID="NotesTextArea" TextMode="multiline" Columns="60" Rows="2" runat="server" Required />
                </fieldset>
            </td>
        </tr>
        <tr>
            <td width="50%" valign="top">
                <fieldset>
                    <legend>Signature</legend>
                    <asp:Label runat="server" ID="lblName" Text="Name" CssClass="formlabelright left"></asp:Label>
                    <asp:TextBox runat="server" ID="txtName" CssClass="fld200" Required></asp:TextBox><br />
                    <asp:Label runat="server" ID="lblDate" Text="Date" CssClass="formlabelright left"></asp:Label>
                    <asp:TextBox runat="server" ID="txtdate" CssClass="fld100" Required></asp:TextBox>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <fieldset style="padding-top: 0px; padding-bottom: 0px">
                    <asp:CheckBox runat="server" ID="chxAgree" value="check" />
                    <p style="display: inline;"><strong>I agree to be a catering vendor in the TradeFirst Barter Expo.</strong></p>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <fieldset style="padding-top: 0px; padding-bottom: 0px">
                    <asp:CheckBox runat="server" ID="chxTerms" value="check" />
                    <p style="display: inline;">
                        <strong>I understand Tradefirst will be providing me with a free space and paying me $1,000 trade to help defray my expenses,
                                and I will be bringing enough food to last for the duration of the show. TradeFirst provides 6� plates, forks, knives, paper
                                napkins and electricity. TradeFirst will be providing wristbands to all TradeFirst members; members must show a wrist
                                band to recieve food.
                        </strong>
                    </p>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Button ID="btnsave" runat="server" Text="Submit" OnClick="btnsave_click" CssClass="btn"
                    ToolTip="Submit" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <fieldset style="padding-top: 0px; padding-bottom: 0px">
                    <p style="display: inline;">
                        <strong>Booth must be set up by 4:00pm.</strong> Booths are assigned at the discretion of TradeFirst. Buyer�s signature acknowledges
                        satisfactory receipt of product or services described and gives authority to TradeFirst to debit buyer�s account. All rules
                        and regulations apply. No shows will be penalized an additional $500 trade if they have not notified TradeFirst two weeks
                        prior to the show.
                    </p>
                </fieldset>
            </td>
        </tr>
    </table>

    <script type="text/javascript">

    </script>

</asp:Content>
