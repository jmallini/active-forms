using System;
using System.Configuration;
using System.Data;
using System.Net.Mail;
using System.Data.SqlClient;


public partial class MI_Holiday_Restaurant_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (System.Web.HttpContext.Current.Session["TradeID"] == null || System.Web.HttpContext.Current.Session["TradeID"] == "")
        {
            Response.Redirect("memberlogin.aspx");
        }

        AfterLoginBL objVendorForm = new AfterLoginBL();
        if (!IsPostBack)
        {

            objVendorForm.trade_id = Convert.ToInt32(Session["UserID"].ToString());
            string itrade_id = objVendorForm.trade_id.ToString();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Tradefirst"].ConnectionString);
            SqlDataAdapter da = new SqlDataAdapter("usp_LoginUserInfo " + itrade_id, con);
            DataTable dt = new DataTable();
            da.Fill(dt);

            txtAccountNum.Text = Session["UserID"].ToString(); 
            txtCompany.Text = dt.Rows[0]["Company_Name"].ToString();
            txtAccountExec.Text = dt.Rows[0]["AE"].ToString();
            txtContactPerson.Text = dt.Rows[0]["SignerName"].ToString();
            txtEmail.Text = dt.Rows[0]["Email"].ToString();
            txtContact.Text = dt.Rows[0]["Phone"].ToString();
            txtdate.Text = System.DateTime.Now.ToString("MM/dd/yyy");
            lblAccountExecEmail.Text = dt.Rows[0]["AccountExecutives"].ToString();

        }


    }
    protected void btnsave_click(object sender, EventArgs e)
    {
        if (chxAgree.Checked == false || chxTerms.Checked == false)
        {
            Response.Write("<script>alert('Agree the Trade First terms by selecting both the checkboxes')</script>");
        }
        else
        {
            string sFrom = ConfigurationManager.AppSettings["sendfrom"].ToString();
            // string sTo = ConfigurationManager.AppSettings["expoadmin"].ToString();
			string sTo = ConfigurationManager.AppSettings["formsAdmin"].ToString();
            string sSubject = "MI RESTAURANT SIGN-UP REQUEST: " + txtCompany.Text;

            if (NotesTextArea.Text == "")
            {
                NotesTextArea.Text = "No description of product bringing";
            };

            if (NotesPrizeArea.Text == "")
            {
                NotesPrizeArea.Text = "No notes on special, discount, raffle, or door prize for more traffic";
            };

            string sBody = "<b>" + "Company Information  " + "</b>" + "<BR/>" + "Company: " + txtCompany.Text + "<BR/>" + "Account Number: " + txtAccountNum.Text + "<BR/>" +
                            "Contact Person: " + txtContactPerson.Text + "<BR/>" + "Phone #: " + txtContact.Text + "<BR/>" + "Email: " + txtEmail.Text + "<BR/>" + "Booth Sign to Read: " + txtBoothSign.Text + "<BR/>" + "<BR/>" + "<b>" + "Sold By" + "</b>" + "<BR/>" +
                            "Sold By: " + txtSold.Text + "<BR/>" + "<BR/>" + "<b>" + "Account Executive" + "</b>" + "<BR/>" + "Account Executive: " + txtAccountExec.Text + "<BR/>" +
                            "<BR/>" + "<b>" + "Product Notes:" + "</b>" + "<BR/>" + NotesTextArea.Text + "<BR/>" + "<BR/>" + "<b>" + "Plan to offer a special, discount, raffle, or door prize for more traffic?: " + "</b>" + "<BR/>" + NotesPrizeArea.Text;

            MailMessage objmail = new MailMessage(sFrom, sTo, sSubject, sBody);
            objmail.To.Add(lblAccountExecEmail.Text);
            objmail.IsBodyHtml = true;
            SmtpClient objclient = new SmtpClient();
            try
            {
                objclient.Send(objmail);
            }
            catch (Exception ex)
            { }
            Response.Redirect("~/MI_Holiday_Restaurant_Request_Sent.aspx");
        }
        
    }
}
