using System;
using System.Configuration;
using System.Data;
using System.Net.Mail;
using System.Data.SqlClient;


public partial class MISpringVendorForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (System.Web.HttpContext.Current.Session["TradeID"] == null || System.Web.HttpContext.Current.Session["TradeID"] == "")
        {
            Response.Redirect("memberlogin.aspx");
        }

        AfterLoginBL objVendorForm = new AfterLoginBL();
        if (!IsPostBack)
        {

            objVendorForm.trade_id = Convert.ToInt32(Session["UserID"].ToString());
            string itrade_id = objVendorForm.trade_id.ToString();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Tradefirst"].ConnectionString);
            SqlDataAdapter da = new SqlDataAdapter("usp_LoginUserInfo " + itrade_id, con);
            DataTable dt = new DataTable();
            da.Fill(dt);

            txtAccountNum.Text = Session["UserID"].ToString(); 
            txtCompany.Text = dt.Rows[0]["Company_Name"].ToString();
            txtAccountExec.Text = dt.Rows[0]["AE"].ToString();
            txtContactPerson.Text = dt.Rows[0]["SignerName"].ToString();
            txtEmail.Text = dt.Rows[0]["Email"].ToString();
            txtContact.Text = dt.Rows[0]["Phone"].ToString();
            txtdate.Text = System.DateTime.Now.ToString("MM/dd/yyy");
            lblAccountExecEmail.Text = dt.Rows[0]["AccountExecutives"].ToString();

        }


    }
    protected void btnsave_click(object sender, EventArgs e)
    {
        string sFrom = ConfigurationManager.AppSettings["sendfrom"].ToString();
        // string sTo = ConfigurationManager.AppSettings["expoadmin"].ToString(); 
		string sTo = ConfigurationManager.AppSettings["formsAdmin"].ToString();
        string sSubject = "VENDOR SIGN-UP REQUEST: " + txtCompany.Text;

        if (NotesTextArea.Text == "")
        {
            NotesTextArea.Text = "No description of product bringing";
        };

        if (NotesPrizeArea.Text == "")
        {
            NotesPrizeArea.Text = "No notes on randomly selected winners on the day of the event";
        };

        string sBody = "<b>" + "Company Information  " + "</b>" + "<BR/>" + "Company: " + txtCompany.Text + "<BR/>" + "Account Number: " + txtAccountNum.Text + "<BR/>" +
                        "Contact Person: " + txtContactPerson.Text + "<BR/>" + "Phone #: " + txtContact.Text + "<BR/>" + "Email: " + txtEmail.Text + "<BR/>" + "Booth Sign to Read: " + txtBoothSign.Text + "<BR/>" + "<BR/>" + "<b>" + "Sold By" + "</b>" + "<BR/>" +
                        "Sold By: " + txtSold.Text + "<BR/>" + "<BR/>" + "<b>" + "Account Executive" + "</b>" + "<BR/>" + "Account Executive: " + txtAccountExec.Text + "<BR/>" +
                        "<BR/>" + "<b>" + "Booth preference" + "</b>" + "<BR/>" + txtCorner.Text + ": corner booth(s) 8�x10� at $475.00 trade" + "<BR/>" + txtBooth.Text + ": booth(s) 8�x10� at $425.00 trade" + "<BR/>" + txtElecticity.Text + ": I would like electricity $25 trade" + "<BR/>" +
                        "<BR/>" + "<b>" + "Table preference" + "</b>" + "<BR/>" + txtSize.Text + ": 6� table or 8� table" + "<BR/>" + txtExtra.Text + ": extra table(s) 6� or 8� $45 trade" + "<BR/>" +
                        "<BR/>" + "<b>" + "Product Notes:" + "</b>" + "<BR/>" + NotesTextArea.Text + "<BR/>" + "<BR/>" + "<b>" + "Door prize to randomly selected winners on the day of the event: " + "</b>" + "<BR/>" + NotesPrizeArea.Text;

        MailMessage objmail = new MailMessage(sFrom, sTo, sSubject, sBody);
        objmail.To.Add(lblAccountExecEmail.Text);
        objmail.IsBodyHtml = true;
        SmtpClient objclient = new SmtpClient();
        try
        {
            objclient.Send(objmail);
        }
        catch (Exception ex)
        { }
        Response.Redirect("~/MISpringVendorRequestSent.aspx");
    }
}
