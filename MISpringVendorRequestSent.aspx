﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AfterLogin.master" AutoEventWireup="false" CodeFile="MISpringVendorRequestSent.aspx.vb" Inherits="MISpringVendorRequestSent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headercontent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" Runat="Server">
    <h3 style="padding:20px">Your expo booth request has been received. You will receive a confirmation email with booth details approximately 3-4 weeks prior to the show. Call your broker anytime with questions, 248-544-1350.</h3>
</asp:Content>

