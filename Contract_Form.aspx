<%@ Page Language="C#" MasterPageFile="~/AfterLogin.master" AutoEventWireup="true"
    CodeFile="Contract_Form.aspx.cs" Inherits="Contract_Form" Title="Contract Form" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headercontent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="maincontent" runat="Server">
    <h1 class="caption">CONTRACTING REQUEST FORM</h1>
    <fieldset>
        We will be more than happy to assist you with your project. We have created the contracting request form in order to match you up with a contractor
        who is best qualified to satisfactorily complete your job. Because of the nature of contracting, things such as job specifications, timing, distance,
        schedules, weather, and cost of goods all must be considered when finding the "right" contractor for your job. In order to help make your project go
        as smoothly as possible, you need to review and follow these contracting guidelines:
        <ul style="padding:20px">
            <li>Tradefirst.com will provide you with a list of contractors that participate with our program.
            </li>
            <li>It is your responsibility to check references and certifications BEFORE you give a deposit or order work to begin.
            </li>
            <li>It is your responsibility to compare quotes to ensure that you are being charged a fair price.
            </li>
            <li>It is your responsibility to inspect the work and make sure you are completely satisfied BEFORE you sign a trade slip or any
                document that states the work is completed.
            </li>
            <li>It is your responsibility to notify TradeFirst immediately if you feel there are problems arising. The sooner we can intervene, the better
                chance of resolving your problem. 
            </li>
        </ul>
        If the project encounters problems, we will do our best to help you resolve whatever issues arise, but it is ultimately up to the parties to resolve
        problems. Too often, members expect TradeFirst to reverse transactions or refund money. There are always two sides to every issue and we are not
        in a position professionally or legally to make a determination. On the rare occasion that your TradeFirst representatives cannot help the parties
        come to an agreement, it will then become necessary to contact an attorney to pursue other options. These incidents are few and far between, we
        simply want you to be fully aware of the policies, so you can proceed with your project in a manner that will ensure a positive outcome.
    </fieldset>

    <table class="width800" style="margin-bottom: 30px;">
        <tr>
            <td valign="top" width="50%">
                <fieldset>
                    <legend>Member Information</legend>
                    <asp:Label ID="lblCompany" runat="Server" Text="Company Name" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtCompany" runat="server" CssClass="fld200" Required></asp:TextBox>
                    <br />
                    <asp:Label ID="lblAccountNum" runat="Server" Text="TF Account #" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtAccountNum" runat="server" CssClass="fld200" ReadOnly="true"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblContactPerson" runat="Server" Text="Contact Name" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtContactPerson" runat="server" CssClass="fld200" Required></asp:TextBox>
                    <br />
                    <asp:Label ID="ldlBid" runat="Server" Text="Bid Deadline" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtBid" runat="server" placeholder="10 days minimum" CssClass="fld200" Required></asp:TextBox>
                    
                    <asp:Label ID="lblAccountExecEmail" runat="Server" class="formlabelright" Visible="false"></asp:Label>
                </fieldset>
            </td>
            <td width="50%" valign="top">
                <fieldset>
                    <legend>Member Information</legend>
                    <asp:Label ID="lblContact" runat="Server" Text="Phone #" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtContact" runat="server" CssClass="fld100" Required></asp:TextBox>
                    <br />
                    <asp:Label ID="lblEmail" runat="Server" Text="E-Mail" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="fld200" Required></asp:TextBox>
                    <br />
                    <asp:Label ID="lblWorkType" runat="Server" Text="Type of work" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtWorkType" runat="server" CssClass="fld200" Required></asp:TextBox>
                    <br />
                    <asp:Label ID="lblDead" runat="Server" Text="Finish Deadline" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtDead" runat="server" CssClass="fld200" Required></asp:TextBox>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <fieldset style="padding-top: 0px; padding-bottom: 0px">
                    <legend>Specifications</legend>
                    <p>(please include all measurements; what's existing; what needs to be done; what the end results
                        should be and any additional information a contractor may need to know about the project. Use second sheet if necessary) </p>
                    <asp:TextBox ID="NotesArea" TextMode="multiline" Columns="100" Rows="6" runat="server" Required />
                </fieldset>
            </td>
        </tr>
        <tr>
            <td valign="top" width="50%">
                <fieldset>
                    <legend>Location Details</legend>
                    <asp:Label ID="lblCity" runat="Server" Text="City" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtCity" runat="server" CssClass="fld200" Required></asp:TextBox>
                    <br />
                    <asp:Label ID="lblLandMark" runat="Server" Text="Cross Streets" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtLandMark" runat="server" CssClass="fld200" Required></asp:TextBox> <br />                    
                </fieldset>
            </td>
            <td valign="top" width="50%">
                <fieldset>
                    <legend>Quote</legend>
                    <asp:Label ID="lblQuote" runat="Server" Text="Cash Quote" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtQuote" runat="server" CssClass="fld200" Required></asp:TextBox>
                    <br />
                    <asp:Label ID="lblQuotedBy" runat="Server" Text="Quoted By" class="formlabelright left"></asp:Label>
                    <asp:TextBox ID="txtQuotedBy" runat="server" CssClass="fld200" Required></asp:TextBox> <br />                    
                </fieldset>
            </td>
        </tr>
        <tr>
            <td width="50%" valign="top">
                <fieldset>
                    <legend>Signature</legend>
                    <asp:Label runat="server" ID="lblName" Text="Name" CssClass="formlabelright left"></asp:Label>
                    <asp:TextBox runat="server" ID="txtName" CssClass="fld200" Required></asp:TextBox><br />
                    <asp:Label runat="server" ID="lblDate" Text="Date" CssClass="formlabelright left"></asp:Label>
                    <asp:TextBox runat="server" ID="txtdate" CssClass="fld100" Required></asp:TextBox>
                </fieldset>
            </td>
        </tr>        
        <tr>
            <td align="right">
                <asp:Button ID="btnsave" runat="server" Text="Submit" OnClick="btnsave_click" CssClass="btn"
                    ToolTip="Submit" />
            </td>
        </tr>
        
    </table>

    <script type="text/javascript">

</script>

</asp:Content>
