using System;
using System.Configuration;
using System.Data;
using System.Net.Mail;
using System.Data.SqlClient;


public partial class Contract_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (System.Web.HttpContext.Current.Session["TradeID"] == null || System.Web.HttpContext.Current.Session["TradeID"] == "")
        {
            Response.Redirect("memberlogin.aspx");
        }

        AfterLoginBL objVendorForm = new AfterLoginBL();
        if (!IsPostBack)
        {

            objVendorForm.trade_id = Convert.ToInt32(Session["UserID"].ToString());
            string itrade_id = objVendorForm.trade_id.ToString();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Tradefirst"].ConnectionString);
            SqlDataAdapter da = new SqlDataAdapter("usp_LoginUserInfo " + itrade_id, con);
            DataTable dt = new DataTable();
            da.Fill(dt);

            txtAccountNum.Text = Session["UserID"].ToString();
            txtCompany.Text = dt.Rows[0]["Company_Name"].ToString();
            txtContactPerson.Text = dt.Rows[0]["SignerName"].ToString();
            txtEmail.Text = dt.Rows[0]["Email"].ToString();
            txtContact.Text = dt.Rows[0]["Phone"].ToString();
            txtdate.Text = System.DateTime.Now.ToString("MM/dd/yyy");
            lblAccountExecEmail.Text = dt.Rows[0]["AccountExecutives"].ToString();

        }


    }
    protected void btnsave_click(object sender, EventArgs e)
    {
        string sFrom = ConfigurationManager.AppSettings["sendfrom"].ToString();
        // string sTo = ConfigurationManager.AppSettings["expoadmin"].ToString();
		string sTo = ConfigurationManager.AppSettings["formsAdmin"].ToString();
        // string sTo = "jmallini@tradeFirst.com";
        string sSubject = "CONTRACTING REQUEST FORM: " + txtCompany.Text;


        string sBody = "<b>" + "Member Information  " + "</b>" + "<BR/>" + "Company: " + txtCompany.Text + "<BR/>" + "Account Number: " + txtAccountNum.Text + "<BR/>" +
                        "Contact Person: " + txtContactPerson.Text + "<BR/>" + "Phone #: " + txtContact.Text + "<BR/>" + "Email: " + txtEmail.Text + "<BR/>" + "Type of work: " + txtWorkType.Text + "<BR/>" + "<BR/>" + "<b>" + "Bid Deadline" + "</b>" + "<BR/>" +
                        "Bid Deadline: " + txtBid.Text + "<BR/>" + "<BR/>" + "<b>" + "Finish Deadline" + "</b>" + "<BR/>" + "Finish Deadline: " + txtDead.Text + "<BR/>" +
                        "<BR/>" + "<b>" + "Specifications:" + "</b>" + "<BR/>" + NotesArea.Text + "<BR/>" + "<BR/>" + "<b>" + "Location Details: " + "</b>" + "<BR/>" + "City: " + txtCity.Text + "<BR/>" + "Cross Streets: " + txtLandMark.Text + "<BR/>" + "<BR/>" + "<b>" + "Quote Details" + "</b>" + "<BR/>" + "Cash Quote: " + txtQuote.Text + "<BR/>" + "Quoted By: " + txtQuotedBy.Text;

        MailMessage objmail = new MailMessage(sFrom, sTo, sSubject, sBody);
        objmail.To.Add(lblAccountExecEmail.Text);
        objmail.IsBodyHtml = true;
        SmtpClient objclient = new SmtpClient();
        try
        {
            objclient.Send(objmail);
        }
        catch (Exception ex)
        { }
        Response.Redirect("~/Contract_Form_Sent.aspx");
    }
}
